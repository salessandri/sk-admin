include_directories(.)


set(SOURCE_CLIENT
  main_client.cpp
  SkAdminClient.cpp
  SkAdminRequest.cpp
  SkAdminHelpers.cpp
)
set(LIBRARIES_CLIENT
  boost_program_options tins cryptopp sqlite3
)

add_executable(SkAdminClient ${SOURCE_CLIENT})
target_link_libraries(SkAdminClient ${LIBRARIES_CLIENT})

set(SOURCE_SERVER
  main_server.cpp
  SkAdminServer.cpp
  SkAdminHelpers.cpp
  SkAdminRequest.cpp
  SkAdminFeedbackSender.cpp
)
set(LIBRARIES_SERVER
  boost_system boost_program_options tins cryptopp sqlite3 ssl crypto
)

add_executable(SkAdminServer ${SOURCE_SERVER})
target_link_libraries(SkAdminServer ${LIBRARIES_SERVER})
