
#include "SkAdminHelpers.hpp"

using namespace skadmin::helpers;

uint32_t skadmin::helpers::char_to_uint32(uint8_t arr[4]) {
    uint32_t result = 0;
    result |= arr[0] << 24;
    result |= arr[1] << 16;
    result |= arr[2] << 8;
    result |= arr[3];
    return result;
}

void skadmin::helpers::uint32_to_char(uint32_t num, uint8_t arr[4]) {
    arr[3] = num & 0xff;
    arr[2] = (num >> 8) & 0xff;
    arr[1] = (num >> 16) & 0xff;
    arr[0] = (num >> 24) & 0xff;
}
