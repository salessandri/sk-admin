
#ifndef __SKADMINSERVER_HPP__
#define __SKADMINSERVER_HPP__

#include <map>
#include <set>
#include <vector>
#include <cstdint>
#include <functional>
#include <chrono>
#include <memory>
#include <string>
#include <thread>

#include <tins.h>

namespace skadmin {
namespace server {

class SkAdminServer {
public:
    
    const static std::string BASE_IV;
    
    SkAdminServer(const std::string& aes_key, const std::string& hmac_key, 
                  const std::string& iv_db);
    void start(std::string iface, uint16_t port);
    void stop();
    virtual ~SkAdminServer();
    
private:

    const static int TAG_SIZE;
    
    std::string iface_;
    Tins::Sniffer* sniffer_;
    uint16_t port_;
    bool running_;
    std::string aes_key_;
    std::string hmac_key_;
    std::string iv_db_;
    
    uint32_t packet_count_;
    std::vector<std::shared_ptr<Tins::TCP>> packets_;
    std::string current_iv_;
    std::function<void(const std::shared_ptr<Tins::TCP>&, uint8_t*)> process_function_;
    std::chrono::time_point<std::chrono::system_clock> max_time_;
    std::map<uint32_t, std::thread> threads;
    uint32_t thread_id;
    std::vector<uint32_t> finished_threads;

    bool sniff_callback(Tins::PDU &pdu);
    bool extract_data_from_packet(const std::shared_ptr<Tins::TCP>& packet, uint8_t *data) const;
    bool validate_packet(const std::shared_ptr<Tins::TCP>& packet, uint8_t* data) const;
    void parse_first_packet(const std::shared_ptr<Tins::TCP>& packet, uint8_t* data);
    void parse_other_packet(const std::shared_ptr<Tins::TCP>& packet, uint8_t* data);
    void clear_used_ivs();
    bool was_used_iv(const std::string& iv);
    void process_requirement();
    void trigger_action(uint32_t id, uint8_t* data);
    void reset_daemon_requirement();
    
};

}
}

#endif
