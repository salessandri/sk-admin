
#include <signal.h>

#include <cstdint>
#include <cstdlib>
#include <string>
#include <functional>

#include <boost/program_options.hpp>
#include <cryptopp/hex.h>

#include "SkAdminServer.hpp"

using namespace std;

using skadmin::server::SkAdminServer;
using CryptoPP::HexDecoder;
using CryptoPP::StringSink;
namespace po = boost::program_options;

std::function<void()> stop_daemon;

void sigint_handler(int) {
    stop_daemon();
}

void decode_hex(const string& hex_encoded, string& result) {
    HexDecoder decoder;
    decoder.Attach(new StringSink(result));
    decoder.Put((byte*)hex_encoded.data(), hex_encoded.size());
    decoder.MessageEnd();
}

int main(int argc, char* argv[]) {
    
    po::options_description opts;
    
    opts.add_options()
        ("iface", po::value<string>()->required(), "Interface to capture the packets from")
        ("port", po::value<uint16_t>()->required(), "Port where the server has to sniff traffic (the one used by the public service)")
        ("hmac-pass", po::value<string>()->required(), "Password to use for the MD5-HMAC (16 bytes in hex)")
        ("aes-pass", po::value<string>()->required(), "Password to use for the AES-GCM cipher (16 bytes in hex)")
        ("iv-db", po::value<string>()->default_value("iv-pass.db"), "DB where used IVs are stored.")
        ;
    
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(opts).run(), vm);
    try {
        po::notify(vm);
    }
    catch (po::error& e) {
        cout << e.what();
        return 1;
    }
    
    uint16_t port = vm["port"].as<uint16_t>();
    string iface = vm["iface"].as<string>();
    string iv_db = vm["iv-db"].as<string>();
    string aes_key;
    decode_hex(vm["aes-pass"].as<string>(), aes_key);
    string hmac_key;
    decode_hex(vm["hmac-pass"].as<string>(), hmac_key);
    
    SkAdminServer skd(aes_key, hmac_key, iv_db);
    stop_daemon = std::bind(&SkAdminServer::stop, &skd);
    signal(SIGINT, sigint_handler);
    skd.start(iface, port);
    
}
