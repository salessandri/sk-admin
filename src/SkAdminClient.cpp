
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <chrono>

#include <cryptopp/gcm.h>
#include <cryptopp/aes.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/filters.h>
#include <cryptopp/hmac.h>
#include <cryptopp/md5.h>
#include <cryptopp/osrng.h>
#include <cryptopp/hex.h>
#include <sqlite3.h>

#include "SkAdminClient.hpp"
#include "SkAdminHelpers.hpp"

using namespace skadmin::client;
using namespace Tins;
using namespace std;

using CryptoPP::HMAC;
using CryptoPP::MD5;
using CryptoPP::GCM;
using CryptoPP::AES;
using CryptoPP::AuthenticatedSymmetricCipher;
using CryptoPP::AuthenticatedEncryptionFilter;
using CryptoPP::AuthenticatedDecryptionFilter;
using CryptoPP::HashFilter;
using CryptoPP::StringSource;
using CryptoPP::ArraySink;
using CryptoPP::AutoSeededRandomPool;
using CryptoPP::HexEncoder;
using CryptoPP::StringSink;

using skadmin::helpers::char_to_uint32;

const int SkAdminClient::TAG_SIZE = 16;
const string SkAdminClient::BASE_IV = "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
const int SkAdminClient::DATA_PER_PACKET = 12;

SkAdminClient::SkAdminClient(const string& aes_key, const string& hmac_key,
                             const string& iv_db) : aes_key_(aes_key), hmac_key_(hmac_key),
                                                    iv_db_(iv_db){
    string create_table_script = "CREATE TABLE IF NOT EXISTS used_ivs (iv BLOB UNIQUE)";
    sqlite3 *db;
    sqlite3_open(iv_db_.c_str(), &db);
    sqlite3_exec(db, create_table_script.c_str(), NULL, NULL, NULL);
    sqlite3_close(db);
}

string SkAdminClient::generate_iv() {
    byte iv[16];
    AutoSeededRandomPool rng;
    rng.GenerateBlock(iv, 16);
    string result((const char*)iv, 16);
    return result;
}

bool SkAdminClient::was_iv_used(const std::string& iv) {
    bool result = false;
    string query = "SELECT * FROM used_ivs WHERE iv = ?1";
    sqlite3 *db;
    sqlite3_stmt *stmt;
    sqlite3_open(iv_db_.c_str(), &db);
    sqlite3_prepare_v2(db, query.c_str(), query.size(), &stmt, NULL);
    sqlite3_bind_blob(stmt, 1, iv.c_str(), 16, SQLITE_STATIC);
    switch (sqlite3_step(stmt)) {
    case SQLITE_DONE:
        break;
    case SQLITE_ROW:
        result = true;
        break;
    default:
        cout << "ERROR!!!\n";
    }
    sqlite3_finalize(stmt);
    // Add IV in case it wsan't used.
    if (!result) {
        string insert_query = "INSERT INTO used_ivs (iv) VALUES (?1)";
        sqlite3_prepare_v2(db, insert_query.c_str(), insert_query.size(), &stmt, NULL);
        sqlite3_bind_blob(stmt, 1, iv.c_str(), 16, SQLITE_STATIC);
        if (sqlite3_step(stmt) != SQLITE_DONE) {
            cout << "ERROR WHEN INSERTING IV" << endl;
        }
        sqlite3_finalize(stmt);
    }
    sqlite3_close(db);
    return result;
}

void SkAdminClient::send(IP::address_type ip, uint16_t port, const SkAdminRequest& request,
                         IP::address_type src_ip) {
    PacketSender ps;
    IP::address_type my_ip;
    if ((uint32_t)src_ip == 0) {
        my_ip = NetworkInterface(ip).addresses().ip_addr;
    }
    else {
        my_ip = src_ip;
    } 
    
    string iv = generate_iv();
    
    while (was_iv_used(iv)) {
        iv = generate_iv();
    }
    
    auto requirement = build_requirement(iv, request);
    for (const auto &packet : requirement) {
        TCP *tcp_packet = packet->clone();
        tcp_packet->dport(port);
        tcp_packet->set_flag(TCP::SYN, 1);
        IP ip_packet(ip, my_ip);
        ip_packet.inner_pdu(tcp_packet);
        ps.send(ip_packet);
        this_thread::sleep_for(chrono::milliseconds(750));
    } 
    
}

vector<shared_ptr<TCP> > SkAdminClient::build_requirement(const std::string& iv, 
                                                          const SkAdminRequest& request) {
    
    byte serialized_request[32];
    request.serialize(serialized_request, 32);
    
    uint8_t enc_data_and_tag[48];
    vector<shared_ptr<TCP> > packets;
    
    GCM<AES>::Encryption encryptioner;
    encryptioner.SetKeyWithIV((byte*)aes_key_.c_str(), 16, (byte*)iv.c_str(), 12);
    
    AuthenticatedEncryptionFilter ef(encryptioner,
                                     new ArraySink(enc_data_and_tag, 48), 
                                     false, 
                                     TAG_SIZE
                                     );
    ef.ChannelPut("AAD", (const byte*)iv.c_str(), 12);
    ef.ChannelMessageEnd("AAD");
    
    ef.ChannelPut("", serialized_request, 32);
    ef.ChannelMessageEnd("");
    
    packets.push_back(build_packet(0, BASE_IV, (byte*)iv.c_str()));
    for (size_t i = 1; i < 5; i++) {
        packets.push_back(build_packet(i, iv, enc_data_and_tag + (i-1) * 12));
    }
    
    return packets;
    
}

shared_ptr<TCP> SkAdminClient::build_packet(uint8_t pos, const string& iv,  uint8_t* data) {
    uint8_t hash_tag[16];
    uint8_t to_hash[24];
    
    memcpy(to_hash, iv.c_str(), 12);
    memcpy(to_hash+12, data, 12);
    
    HMAC<MD5> hmac((const byte*)hmac_key_.c_str(), 16);
    StringSource(to_hash,
                 24,
                 true,
                 new HashFilter(hmac,
                                new ArraySink(hash_tag, 16)
                                )
                );
    
    uint8_t ts_opt_data[8];
    memset(ts_opt_data, 0, 8);
    memcpy(ts_opt_data, data+8, 4);
    TCP::option ts_opt(TCP::TSOPT, 8, ts_opt_data);
    
    uint8_t tcpauth_opt_data[18];
    memset(tcpauth_opt_data, 0, 18);
    tcpauth_opt_data[0] = 0x80;
    tcpauth_opt_data[1] = 0xff;
    memcpy(tcpauth_opt_data+2, hash_tag, 16);
    TCP::option tcpauth_opt(29, 18, tcpauth_opt_data);
    
    auto packet = make_shared<TCP>();
    packet->sport(12903 + pos);
    packet->seq(char_to_uint32(data));
    packet->ack_seq(char_to_uint32(data+4));
    packet->add_option(ts_opt);
    packet->add_option(tcpauth_opt);
    packet->window(15000 + pos);

    return packet;
    
}

