
#include "SkAdminFeedbackSender.hpp"

using namespace std;
using namespace skadmin::server;

using boost::asio::ip::tcp;
namespace ssl = boost::asio::ssl;

boost::asio::io_service SkAdminFeedbackSender::io_service;

SkAdminFeedbackSender::SkAdminFeedbackSender(const Tins::IPv4Address ip, uint16_t port) : ip_(ip), 
        port_(port), connected_(false) {
}
    
void SkAdminFeedbackSender::connect() {
    try {
        ssl::context ctx(ssl::context::sslv23);
        socket_ = make_shared<ssl_socket>(io_service, ctx);
        auto address = boost::asio::ip::address::from_string(ip_.to_string());
        tcp::endpoint remote_endpoint(address, port_);
        socket_->lowest_layer().connect(remote_endpoint);
        socket_->handshake(ssl_socket::client);
        connected_ = true;
    }
    catch (boost::exception const& e) {
        connected_ = false;
    }
}

void SkAdminFeedbackSender::send(const uint8_t *buffer, size_t size) {
    if (!connected_)
        return;
    socket_->write_some(boost::asio::buffer(buffer, size));
}

void SkAdminFeedbackSender::close() {
    if (!connected_)
        return;
    boost::system::error_code ec;
    socket_->shutdown(ec);
}

SkAdminFeedbackSender::~SkAdminFeedbackSender() {
    
}
