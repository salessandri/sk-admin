
#ifndef SKADMINFEEDBACKSENDER_HPP_
#define SKADMINFEEDBACKSENDER_HPP_

#include <memory>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <ip_address.h>

namespace skadmin {
namespace server {

class SkAdminFeedbackSender {
public:
    
    SkAdminFeedbackSender(const Tins::IPv4Address ip, uint16_t port);
    
    void connect();
    
    void send(const uint8_t *buffer, size_t size);
    
    void close();
    
    ~SkAdminFeedbackSender();
    
protected:

    static boost::asio::io_service io_service;
    
    using ssl_socket = boost::asio::ssl::stream<boost::asio::ip::tcp::socket>;
    
    Tins::IPv4Address ip_;
    uint16_t port_;
    std::shared_ptr<ssl_socket> socket_;
    bool connected_;
    
};

}
}


#endif 
