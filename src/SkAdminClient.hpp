
#ifndef __SKC_HPP__
#define __SKC_HPP__

#include <map>
#include <set>
#include <vector>
#include <cstdint>
#include <functional>
#include <chrono>
#include <memory>
#include <string>
#include <thread>

#include <tins.h>

#include "SkAdminRequest.hpp"

namespace skadmin {
namespace client {

class SkAdminClient {

public:
    SkAdminClient(const std::string& aes_key, const std::string& hmac_key, 
                  const std::string& iv_db);
    void send(Tins::IP::address_type ip, uint16_t port, const SkAdminRequest& request,
              Tins::IP::address_type src_ip = 0);
    
private:
    
    const static int TAG_SIZE;
    const static std::string BASE_IV;
    const static int DATA_PER_PACKET;
    
    std::string aes_key_;
    std::string hmac_key_;
    std::string iv_db_;
    
    std::string generate_iv();
    bool was_iv_used(const std::string& iv);
    std::shared_ptr<Tins::TCP> build_packet(uint8_t pos, const std::string& iv,  uint8_t* data);
    std::vector<std::shared_ptr<Tins::TCP> > build_requirement(const std::string& iv, 
                                                               const SkAdminRequest& request);
    

};

}
}

#endif
