
#ifndef SKADMINHELPERS_HPP_
#define SKADMINHELPERS_HPP_

#include <cstdint>

namespace skadmin {
namespace helpers {

uint32_t char_to_uint32(uint8_t arr[4]); 

void uint32_to_char(uint32_t num, uint8_t arr[4]);

}
}


#endif 
